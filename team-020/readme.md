###Pesquisa sobre versões e controlo de código

####Descrição do concern
Um sistema de controlo de versões tem como objetivo armazenar e gerir informação, assim como de manter versões, sempre que a informação é alterada.

Na área da engenharia informática, sistemas de controlo de versões como o [git](https://git-scm.com/), são frequentemente utilizados entre equipas, de forma a armazenar e atualizar software que esteja em desenvolvimento, gerir versões ou outras funcionalidades que o sistemas suporte. Os sistemas de controlo de versões são portanto, quase essenciais para o desenvolvimento de software, particularmente entre equipas de desenvolvimento com vários membros.

Relativamente à área em estudo na unidade curricular, a engenharia de domínio, estes sistemas são também utilizados, no entanto existem determinados sistemas de controlo (e.g. [EMFStore](https://www.eclipse.org/emfstore/) em EMF) que são usados particularmente nesta área.

####Descrição do approach
Outras plataformas de desenvolvimento de DSL como o [MPS](https://www.jetbrains.com/mps/) ou o [Microsoft DSL SDK](https://docs.microsoft.com/en-us/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2017) fornecem ferramentas que facilitam o uso de controlo de versões, como o git.

Ambas as plataformas permitem executar a maioria das funcionalidades do git, através de menus.

![](https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/share-your-code-in-git-vs/vs-status-bar.png?view=vsts)
######Git no Visual Studio

![](https://www.jetbrains.com/help/img/idea/2018.2/Git_file_status.png)
######Git no MPS


Existem plataformas de modelação que contêm os seus próprios sistemas de controlo de versões, desenvolvidos em específico para serem utilizados no contexto da plataforma, como é o caso do EMFStore em projetos desenvolvidos em EMF.

O EMFStore é um repositório de modelos, que permite a gestão de versões de modelos desenvolvidos pelo EMF. Como esta ferramenta tem como objetivo ser utilizada apenas em soluções de EMF, tem diversas funcionalidades específicas, como o de resolver conflitos ou "merging" de forma mais eficaz, visto o controlo de versões ser baseada em modelos. Isto significa que enquanto em sistemas como o git se adicionam ou editam ficheiros, com o EMFStore são adicionados ou editados modelos ou elementos de um modelo.

####Aplicação do approach (EMFStore)

#####Pŕe-requisitos
-[Instalar o EMFStore, via marketplace do eclipse](https://marketplace.eclipse.org/content/emfstore-0#.UdvdOvn0Fn8).
-Criar um projeto ou utilizar um já existente em EMF que será utilizado para testar a plataforma.

#####Aplicação
-Criar uma instância no Eclipse a partir do modelo a ser utilizado, com uma configuração Eclipse Application.
-É necessário para além do demonstrado na imagem seguinte, adicionar como plug-in o projeto de modelação base e o projeto *.edit correspondente.

![](https://lh5.googleusercontent.com/I8M1SvrZbhz_i5g7ggpSnQSxO4RTOT2bZ5XUxF2cc7KfG4USmfQ9_bkNeUfDdEPkKD_6AjF0qLolHCsTC7-9JRkbezuaRNs380qXj4K57ayS608vSGVzN8C18Fjk6g7_AA)

-Na nova instância criada a partir desta configuração, criar um novo projeto e adicionar um elemento do modelo.

![](https://lh3.googleusercontent.com/edOPXz1ue9oWgmlFet5fuJHsdpW8rMPzUjoBGnCCdgudci0wocJ7dZ1pnLh4Fq1MifD6AXWG73xnufo1zTO6316Tvot279uyzFhVtGbA5kC_dea97ACScAjZGqK5G_0YQQ)

![](https://lh6.googleusercontent.com/rPuwXYLpFCU_GYltNPqXNKlbKrYBFWTucdX83FndQ150t4l1UvFjiid3DtMyD9GoJE_1bkDgo6mTzUoJLN8G8moPKsW87ZCpXapX5U0fc0k5YPf9O9bB4O8etiGkQW7NQg)

-Criar um servidor local na aba *Model Repositories*.

![](https://lh6.googleusercontent.com/ZfxD59CZoe_fQ0J_RVa_8ZKCKKhjLPbWklZHA6ZViPM8lZCoBQ1zGyA_yTgEWbs68ap3A3ZOCKJagUn6ZFPYFDcmh6Cp14XkhE61fFx9GeRq6yJ7TnM1YIjTbRZnxU05IQ)

-Partilhar o servidor, para que possa ser utilizado como respositório de modelos.
-Fazer uma alteração numa instância de modelo criada e fazer commit.

![](https://lh4.googleusercontent.com/eS1nVecFyLoTsXb0fBBJv-Q7jD58BhUTueA1rjQ2P3H9LQuRCY9--XsI4dx-B85Rdjy98hroR3XQMxiskM6qnzLfhnIC9t7PXkM0Dlcy5AcbYo-uuoYCBNcbYkotMJ_PkQ)

Para testar a resolução de conflitos de *merge*, podem-se efetuar os passos seguintes:

-Selecionar a opção *Checkout Branch...* conforme indicado na imagem seguinte, para fazer uma cópia do projeto, simulando assim um segundo utilizador.

![](https://lh4.googleusercontent.com/wPj6HhvGvDSUazsGnuDJ3eX9Qo9DKOyQwU4FRnWTEZXcRiE7usECDlYLinVCXyajUUremxR76Fq6je9-1TsQsrsMFmf1OT3XpAjlNEkvr7V8OGjOyUSeLsvwbQ7pUB6YGA)

-Atualizar um mesmo elemento, num mesmo atributo, em cada um dos dois projetos e fazer commit.
-Deverá de aparecer um *Wizard* para resolver conflitos num dos projetos.
-Selecionar *Keep My Change*

![](https://lh4.googleusercontent.com/01LuY8ebf-aLeEJMnCHKBau06s1Oe1Z7V0kLj-JsMtoCRxmy8lo6GCkrd6_e0XPCqj4E_6dRNMkX-A1pcpQCehUNb0gdGYWCP9o1uIzTn7WZfPOYXi9gQ4zMQfdvwEjfgg)

####Comparação entre os sistemas de controlo de versões
Visto o sistema de controlo de versões nesta unidade curricular ter sido o git, esta secção compara o git com o repositório do EMFStore.

O facto de o sistema de controlo de versões do EMFStore ser focado especificamente para projetos feitos em EMF e de, consequentemente, fornecer funcionalidades de gestão de modelos faz com que essa seja uma grande vantagem quando comparado com sistemas como o git.
A resolução de conflitos parece ser também mais eficaz no EMFStore, visto comparar versões de modelos/elementos/atributos, enquanto que utilizando o git, o mesmo caso de uso seria resolvido comparando o conteúdo dos ficheiros correspondentes.
Uma possível desvantagem da utilização do EMFStore é a de ser, por definição, apenas utilizado em projetos EMF. Isto significa que uma aplicação que para além de modelação em EMF, contivesse outros projetos (e.g. backend), seria necessário utilizar mais que um sistema de controlo de versões (assumindo que utilizasse EMFStore no projeto em EMF).

Em suma, apesar de numa aplicação com vários projetos diferentes perder valor, a utilização de respositórios do EMFStore parece ser uma mais-valia em projetos realizados em EMF.

####Referências
[EMFStore](https://www.eclipse.org/emfstore/)
[MPS version-control-integration](https://www.jetbrains.com/help/mps/version-control-integration.html)
[Visual Studio version-control-integration](https://git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Visual-Studio)
[EMFStore Tutorial](http://www.eclipsesource.com/blogs/tutorials/getting-started-with-emfstore)
